import React, { useEffect } from 'react';
import './App.css';
import UserList from './components/UserList';
import ErrorBoundary from './container/ErrorBoundary';
import { useDispatch } from 'react-redux';
import { loadUsers } from './actions/userActions';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadUsers());
  }, [dispatch]);

  return (
    <div className="App">
    <main>
      <ErrorBoundary>
        <UserList />
      </ErrorBoundary>
    </main>
  </div>
  );
};

export default App;
