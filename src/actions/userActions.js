import axios from 'axios';

export const loadUsers = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get('/data/users.json');
      dispatch({
        type: 'LOAD_USERS',
        payload: response.data,
      });
    } catch (error) {
      console.error('Error loading users:', error);
    }
  };
};

export const addUser = (user) => ({
  type: 'ADD_USER',
  payload: user,
});

export const deleteUser = (userId) => ({
  type: 'DELETE_USER',
  payload: userId,
});

export const detailUser = (user) => ({
  type: 'DETAIL_USER',
  payload: user,
});

export const editUser = (user) => ({
  type: 'EDIT_USER',
  payload: user,
});
