import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteUser, selectUser } from '../actions/userActions';
import Button from 'react-bootstrap/Button';

const UserActions = ({ user }) => {
  const dispatch = useDispatch();

  const handleEdit = () => {
    dispatch(selectUser(user));
  };

  const handleDelete = () => {
    dispatch(deleteUser(user.id));
  };

  return (
    <div>
        <Button variant="outline-primary" onClick={handleEdit}>Edit</Button>{' '}
        <Button variant="outline-danger" onClick={handleDelete}>Delete</Button>{' '}
    </div>
  );
};

export default UserActions;
