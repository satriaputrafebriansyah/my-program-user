import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUser, addUser, editUser } from '../actions/userActions';
import { Table, Button, FormControl, InputGroup } from 'react-bootstrap';
import ModalEdit from '../container/ModalEdit';
import ModalAdd from '../container/ModalAdd';
import ModalDetail from '../container/ModalDetail';

const UserList = () => {
  const dispatch = useDispatch();
  const users = useSelector(state => state.user.users);
  const [showEdit, setShowEdit] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showDetail, setShowDetail] = useState(false);
  const [currentUser, setCurrentUser] = useState({ id: '', name: '', username: '', email: '', phone: '', website: '', address: { street: '', city: '' } });
  const [searchTerm, setSearchTerm] = useState('');
  const [sortBy, setSortBy] = useState({ column: null, direction: 'asc' });

  const handleClose = () => {
    setShowEdit(false);
    setShowAdd(false);
    setShowDetail(false);
    setCurrentUser({ id: '', name: '', username: '', email: '', phone: '', website: '', address: { street: '', city: '' } });
  };

  const handleShowEdit = (user) => {
    setCurrentUser(user);
    setShowEdit(true);
  };

  const handleShowAdd = () => {
    setCurrentUser({ id: '', name: '', username: '', email: '', phone: '', website: '', address: { street: '', city: '' } });
    setShowAdd(true);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name.startsWith('address.')) {
      setCurrentUser({
        ...currentUser,
        address: {
          ...currentUser.address,
          [name.split('.')[1]]: value
        }
      });
    } else {
      setCurrentUser({ ...currentUser, [name]: value });
    }
  };

  const handleSaveChanges = () => {
    dispatch(editUser(currentUser));
    handleClose();
  };

  const handleDelete = (id) => {
    dispatch(deleteUser(id));
  };

  const handleAddUser = () => {
    const maxId = Math.max(...users.map(user => user.id), 0);
    const newUser = { ...currentUser, id: maxId + 1 };
    dispatch(addUser(newUser));
    handleClose();
  };

  const handleShowDetail = (user) => {
    setCurrentUser(user);
    setShowDetail(true);
  };

  const handleSort = (column) => {
    const direction = sortBy.column === column && sortBy.direction === 'asc' ? 'desc' : 'asc';
    setSortBy({ column, direction });
  };

  const sortedUsers = users.sort((a, b) => {
    if (sortBy.direction === 'asc') {
      return a[sortBy.column] < b[sortBy.column] ? -1 : 1;
    } else {
      return a[sortBy.column] > b[sortBy.column] ? -1 : 1;
    }
  });

  const filteredUsers = sortedUsers.filter(user =>
    user.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.username.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.email.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.phone.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.website.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.address.city.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.address.street.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="container-fluid mt-4">
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h1 className='text-left'>User List</h1>
        <Button variant="success" onClick={handleShowAdd}>Add User</Button>
      </div>
      <InputGroup className="mb-3">
        <FormControl
          placeholder="Search..."
          aria-label="Search"
          aria-describedby="basic-addon2"
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </InputGroup>
      {filteredUsers.length > 0 ? (
        <div className="table-responsive">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th onClick={() => handleSort('id')}>ID</th>
                <th onClick={() => handleSort('name')}>Name</th>
                <th onClick={() => handleSort('username')}>Username</th>
                <th onClick={() => handleSort('email')}>Email</th>
                <th onClick={() => handleSort('phone')}>Phone</th>
                <th onClick={() => handleSort('website')}>Website</th>
                <th onClick={() => handleSort('address.city')}>City</th>
                <th onClick={() => handleSort('address.street')}>Street</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {filteredUsers.map(user => (
                <tr key={user.id}>
                  <td>{user.id}</td>
                  <td>{user.name}</td>
                  <td>{user.username}</td>
                  <td>{user.email}</td>
                  <td>{user.phone}</td>
                  <td>{user.website}</td>
                  <td>{user.address.city}</td>
                  <td>{user.address.street}</td>
                  <td>
                    <Button variant="danger" className="mr-2" onClick={() => handleDelete(user.id)}>Delete</Button>
                    <Button variant="warning" className="mr-2" onClick={() => handleShowEdit(user)}>Edit</Button>
                    <Button variant="info" className="mr-2" onClick={() => handleShowDetail(user)}>Show</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      ) : (
        <div className="text-center">No users found.</div>
      )}
      <ModalEdit show={showEdit} handleClose={handleClose} handleSaveChanges={handleSaveChanges} handleChange={handleChange} currentUser={currentUser} />
      <ModalAdd show={showAdd} handleClose={handleClose} handleAddUser={handleAddUser} handleChange={handleChange} currentUser={currentUser} />
      <ModalDetail show={showDetail} handleClose={handleClose} currentUser={currentUser} />
    </div>
  );
};

export default UserList;
