import React, { useState, useEffect } from 'react';
import { Button, Modal as BootstrapModal } from 'react-bootstrap';

const ModalDetail = ({ handleClose, show, currentUser }) => {
  const [userImage, setUserImage] = useState(null);

  useEffect(() => {
    fetchUserImage(currentUser.id);
  }, [currentUser.id]);

  const fetchUserImage = async (userId) => {
    try {
      const response = await fetch(`https://picsum.photos/id/${userId}/info`);
      const data = await response.json();
      setUserImage(data.download_url);
    } catch (error) {
      console.error('Error fetching user image:', error);
    }
  };

  const address = `${currentUser.address.street}, ${currentUser.address.city}`;

  return (
    <BootstrapModal show={show} onHide={handleClose}>
      <BootstrapModal.Header closeButton>
        <BootstrapModal.Title>User Detail</BootstrapModal.Title>
      </BootstrapModal.Header>
      <BootstrapModal.Body>
        <div className="user-details">
          <div className="user-image mb-3">
            {userImage && <img className='w-100' src={userImage} alt="User" />}
          </div>
          <div className="user-info">
            <p><strong>Name:</strong> {currentUser.name}</p>
            <p><strong>Username:</strong> {currentUser.username}</p>
            <p><strong>Email:</strong> {currentUser.email}</p>
            <p><strong>Address:</strong> {address}</p>
          </div>
        </div>
      </BootstrapModal.Body>
      <BootstrapModal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </BootstrapModal.Footer>
    </BootstrapModal>
  );
};

export default ModalDetail;
