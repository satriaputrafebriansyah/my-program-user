const initialState = {
  users: [],
  selectedUser: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_USERS':
      return {
        ...state,
        users: action.payload
      };
    case 'ADD_USER':
      return {
        ...state,
        users: [...state.users, action.payload]
      };
    case 'DELETE_USER':
      return {
        ...state,
        users: state.users.filter(user => user.id !== action.payload),
        selectedUser: null
      };
    case 'SELECT_USER':
      return {
        ...state,
        selectedUser: action.payload
      };
    case 'EDIT_USER':
      return {
        ...state,
        users: state.users.map(user =>
          user.id === action.payload.id ? { ...user, ...action.payload } : user
        )
      };
    default:
      return state;
  }
};

export default userReducer;
